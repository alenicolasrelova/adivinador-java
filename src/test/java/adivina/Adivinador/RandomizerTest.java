package adivina.Adivinador;

import java.util.ArrayList;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RandomizerTest {

	Randomizer r1 = new Randomizer();
	Randomizer r2 = new Randomizer();
	Randomizer r3 = new Randomizer();
	Randomizer r4 = new Randomizer();
	Randomizer r5 = new Randomizer();
	Randomizer r6 = new Randomizer();
	Randomizer r7 = new Randomizer();
	Randomizer r8 = new Randomizer();
	
	/*ArrayList<Randomizer> rs = new ArrayList<Randomizer>();

	@Before(value = "")
	public void carga() {
		for(int i=0;i<10;i++) {
			rs.add(new Randomizer());
		}
	}*/
	
	@Test
	public void probar1() {
		Assert.isTrue(r1.getRandomizer()<=100 && r1.getRandomizer()>=0,"Fuera de rango");//(r.getRandomizer()<=100 && r.getRandomizer()>=0);
	}
	
	@Test
	public void probar2() {
		Assert.isTrue(r2.getRandomizer()<=100 && r2.getRandomizer()>=0,"Fuera de rango");//(r.getRandomizer()<=100 && r.getRandomizer()>=0);
	}
	
	@Test
	public void probar3() {
		Assert.isTrue(r3.getRandomizer()<=100 && r3.getRandomizer()>=0,"Fuera de rango");//(r.getRandomizer()<=100 && r.getRandomizer()>=0);
	}
	
	@Test
	public void probar4() {
		Assert.isTrue(r4.getRandomizer()<=100 && r4.getRandomizer()>=0,"Fuera de rango");//(r.getRandomizer()<=100 && r.getRandomizer()>=0);
	}
	
	@Test
	public void probar5() {
		Assert.isTrue(r5.getRandomizer()<=100 && r5.getRandomizer()>=0,"Fuera de rango");//(r.getRandomizer()<=100 && r.getRandomizer()>=0);
	}
	
	@Test
	public void probar6() {
		Assert.isTrue(r6.getRandomizer()<=100 && r6.getRandomizer()>=0,"Fuera de rango");//(r.getRandomizer()<=100 && r.getRandomizer()>=0);
	}
	
	@Test
	public void probar7() {
		Assert.isTrue(r7.getRandomizer()<=100 && r7.getRandomizer()>=0,"Fuera de rango");//(r.getRandomizer()<=100 && r.getRandomizer()>=0);
	}
	
	@Test
	public void probar8() {
		Assert.isTrue(r8.getRandomizer()<=100 && r8.getRandomizer()>=0,"Fuera de rango");//(r.getRandomizer()<=100 && r.getRandomizer()>=0);
	}
}
