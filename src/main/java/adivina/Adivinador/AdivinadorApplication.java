package adivina.Adivinador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdivinadorApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdivinadorApplication.class, args);
	}
}
