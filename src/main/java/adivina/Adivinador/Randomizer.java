package adivina.Adivinador;
import java.util.Random;

public class Randomizer {
	Random randomGenerator = new Random();
	private int adivinanza;
	
	public Randomizer() {
		adivinanza = randomGenerator.nextInt(100);
	}
	
	public int getRandomizer() {
		return adivinanza;
	}
}
