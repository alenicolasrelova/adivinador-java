package adivina.Adivinador;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/app")
public class AdivinanzaController {
	@RequestMapping("/adivina")
	public String adivina() {
		return "index";
	}

}
